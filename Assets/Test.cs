﻿using UnityEngine;

public class Test : MonoBehaviour {

    public MonoBehaviour other;

    private void Update() {
        if(Input.GetKeyDown(KeyCode.Escape))
            other.enabled = !other.enabled;
    }

}