﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(FrequencyRange))]
public class FrequencyRangeInspector : PropertyDrawer {

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        return EditorGUIUtility.singleLineHeight * 3f;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        var labelRect = position;
        var minRect = position;
        var maxRect = position;

        var minProp = property.FindPropertyRelative("minimum");
        var maxProp = property.FindPropertyRelative("maximum");

        labelRect.yMax -= EditorGUIUtility.singleLineHeight * 2f;
        minRect.yMin += EditorGUIUtility.singleLineHeight;
        minRect.yMax -= EditorGUIUtility.singleLineHeight;
        maxRect.yMin += EditorGUIUtility.singleLineHeight * 2f;

        minRect.xMin += 15f;
        maxRect.xMin += 15f;

        EditorGUI.LabelField(labelRect, label);
        EditorGUI.PropertyField(minRect, minProp);

        minProp.floatValue = minProp.floatValue;

        if(minProp.floatValue > maxProp.floatValue)
            minProp.floatValue = maxProp.floatValue;

        EditorGUI.PropertyField(maxRect, maxProp);

        if(maxProp.floatValue < minProp.floatValue)
            maxProp.floatValue = minProp.floatValue;
    }

}