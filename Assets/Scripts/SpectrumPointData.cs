﻿namespace Visualization {
    public struct SpectrumPointData {
        public int SpectrumPointIndex { get; set; }
        public double Value { get; set; }

        public SpectrumPointData(int spectrumPointIndex, double value) {
            SpectrumPointIndex = spectrumPointIndex;
            Value = value;
        }
    }
}