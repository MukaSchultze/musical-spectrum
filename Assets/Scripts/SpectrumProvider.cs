﻿using System;
using System.Collections.Generic;
using CSCore.DSP;

namespace Visualization {
    public class SpectrumProvider : FftProvider {

        public int SampleRate { get; private set; }

        private readonly List<object> _contexts = new List<object>();

        public SpectrumProvider(int channels, int sampleRate, FftSize fftSize) : base(channels, fftSize) {
            if(sampleRate <= 0)
                throw new ArgumentOutOfRangeException("sampleRate", "Sample rate must be greater than zero.");

            SampleRate = sampleRate;
        }

        public int GetFftBandIndex(float frequency) {
            var fftSize = (int)FftSize;
            var f = SampleRate / 2.0f;

            return (int)((frequency / f) * (fftSize / 2));
        }

        public bool GetFftData(float[] fftBuffer, object context) {
            if(_contexts.Contains(context))
                return false;

            _contexts.Add(context);
            GetFftData(fftBuffer);
            return true;
        }

        public override void Add(float[] samples, int count) {
            base.Add(samples, count);

            if(count > 0)
                _contexts.Clear();
        }

        public override void Add(float left, float right) {
            base.Add(left, right);
            _contexts.Clear();
        }

    }
}