﻿using UnityEngine;

namespace Visualization {
    public class BarSpectrum : SpectrumBase {

        [SerializeField]
        private float barWidth = 1f;
        [SerializeField]
        private float barHeight = 1f;
        [SerializeField]
        private float distanceBetweenBars = 1.2f;
        [SerializeField]
        private GameObject barPrefab;
        private ObjectPool<Transform> barPool;

        private void Awake() {
            barPrefab.SetActive(false);
            barPool = new ObjectPool<Transform>(barPrefab.transform, SpectrumResolution, transform);
        }

        protected override void SpectrumPointCalculated(float value, int index) {
            var bar = barPool.GetFree();

            bar.transform.localScale = new Vector3(barWidth, value * barHeight, 1f);
            bar.transform.position = Vector3.left * SpectrumResolution * distanceBetweenBars / 2f + Vector3.right * index * distanceBetweenBars;
        }

        protected override void Update() {
            foreach(var bar in barPool.objects)
                barPool.SetFree(bar);

            base.Update();
        }

    }
}