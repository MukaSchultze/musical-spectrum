﻿using System;
using UnityEngine;

[Serializable]
public struct FrequencyRange {

    [SerializeField, Range(20f, 20000f)]
    private float minimum;
    [SerializeField, Range(20f, 20000f)]
    private float maximum;

    public FrequencyRange(float min, float max) {
        if(min > max)
            throw new ArgumentException("Minimum value can't be larger than Maximum", "min");

        minimum = min;
        maximum = max;
    }

    public float Minimum {
        get { return minimum; }
        set { minimum = Mathf.Min(value, maximum); }
    }

    public float Maximum {
        get { return maximum; }
        set { maximum = Mathf.Max(value, minimum); }
    }

    public override string ToString() { return string.Format("{0:0}Hz-{1:0}Hz", Minimum, Maximum); }

}