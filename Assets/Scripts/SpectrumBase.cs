﻿using System;
using System.Collections.Generic;
using CSCore;
using CSCore.DSP;
using CSCore.SoundIn;
using CSCore.Streams;
using UnityEngine;

namespace Visualization {

    public enum ScalingStrategy { Decibel, Linear, Sqrt }

    public abstract class SpectrumBase : MonoBehaviour {
        private const int SCALE_LINEAR = 9;
        private const int SCALE_SQR = 2;
        private const double MIN_DB_VALUE = -90d;
        private const double MAX_DB_VALUE = 0d;
        private const double DB_SCALE = MAX_DB_VALUE - MIN_DB_VALUE;

        private readonly List<SpectrumPointData> points = new List<SpectrumPointData>();

        [SerializeField, Range(0f, 500f)]
        private int resolution = 200;
        [SerializeField]
        private ScalingStrategy scalingStrategy = ScalingStrategy.Linear;
        [SerializeField]
        private FrequencyRange m_frequency = new FrequencyRange(20f, 20000f);

        private WasapiCapture m_capture;
        private SpectrumProvider m_spectrumProvider;
        private bool m_isXLogScale;

        private int m_fftSize;
        private int m_maxFftIndex;

        private int m_maximumFrequencyIndex;
        private int m_minimumFrequencyIndex;
        private int[] m_spectrumIndexMax;
        private int[] m_spectrumLogScaleIndexMax;

        public SpectrumBase() {
            m_fftSize = (int)FftSize.Fft4096;
            m_maxFftIndex = m_fftSize / 2 - 1;
            UseAverage = true;
            m_isXLogScale = false;
        }

        public int SpectrumResolution {
            get { return resolution; }
            set { resolution = value; UpdateFrequencyMapping(); }
        }

        public FrequencyRange Frequency {
            get { return m_frequency; }
            set { m_frequency = value; }
        }

        public SpectrumProvider SpectrumProvider {
            get { return m_spectrumProvider; }
            set {
                if(value == null)
                    throw new ArgumentNullException("value");
                else
                    m_spectrumProvider = value;
            }
        }

        public bool IsXLogScale {
            get { return m_isXLogScale; }
            set {
                m_isXLogScale = value;
                UpdateFrequencyMapping();
            }
        }

        public ScalingStrategy ScalingStrategy {
            get { return scalingStrategy; }
            set { scalingStrategy = value; }
        }

        public bool UseAverage {
            get;
            set;
        }

        public FftSize FftSize {
            get { return (FftSize)m_fftSize; }
            set {
                if((int)Mathf.Log((int)value, 2f) % 1 != 0)
                    throw new ArgumentOutOfRangeException("value");

                m_fftSize = (int)value;
                m_maxFftIndex = m_fftSize / 2 - 1;
            }
        }

        public virtual void UpdateFrequencyMapping() {
            if(SpectrumProvider == null) {
                OnDisable();
                OnEnable();
            }

            m_maximumFrequencyIndex = Mathf.Min(SpectrumProvider.GetFftBandIndex(Frequency.Maximum) + 1, m_maxFftIndex);
            m_minimumFrequencyIndex = Mathf.Min(SpectrumProvider.GetFftBandIndex(Frequency.Minimum), m_maxFftIndex);

            var actualResolution = SpectrumResolution;
            var indexCount = m_maximumFrequencyIndex - m_minimumFrequencyIndex;
            var linearIndexBucketSize = Math.Round(indexCount / (double)actualResolution, 3);
            var maxLog = Mathf.Log(actualResolution, actualResolution);

            m_spectrumIndexMax = m_spectrumIndexMax.CheckBuffer(actualResolution, true);
            m_spectrumLogScaleIndexMax = m_spectrumLogScaleIndexMax.CheckBuffer(actualResolution, true);

            for(var i = 1; i < actualResolution; i++) {
                var logIndex = (int)((maxLog - Math.Log((actualResolution + 1) - i, (actualResolution + 1))) * indexCount) + m_minimumFrequencyIndex;

                m_spectrumIndexMax[i - 1] = m_minimumFrequencyIndex + (int)(i * linearIndexBucketSize);
                m_spectrumLogScaleIndexMax[i - 1] = logIndex;
            }

            if(actualResolution > 0)
                m_spectrumIndexMax[m_spectrumIndexMax.Length - 1] = m_spectrumLogScaleIndexMax[m_spectrumLogScaleIndexMax.Length - 1] = m_maximumFrequencyIndex;
        }

        public virtual List<SpectrumPointData> CalculateSpectrumPoints() {
            var fftBuffer = new float[(int)FftSize];
            var value0 = 0d;
            var value = 0d;
            var lastValue = 0d;
            var maxValue = 100d;
            var actualMaxValue = maxValue;
            var spectrumPointIndex = 0;

            if(!SpectrumProvider.IsNewDataAvailable)
                return points;

            points.Clear();
            SpectrumProvider.GetFftData(fftBuffer);

            for(var i = m_minimumFrequencyIndex; i <= m_maximumFrequencyIndex; i++) {
                switch(ScalingStrategy) {
                    case ScalingStrategy.Decibel:
                        value0 = ((20d * Mathf.Log10(fftBuffer[i]) - MIN_DB_VALUE) / DB_SCALE) * actualMaxValue;
                        break;

                    case ScalingStrategy.Linear:
                        value0 = fftBuffer[i] * SCALE_LINEAR * actualMaxValue;
                        break;

                    case ScalingStrategy.Sqrt:
                        value0 = Mathf.Sqrt(fftBuffer[i]) * SCALE_SQR * actualMaxValue;
                        break;

                    default:
                        throw new InvalidOperationException("Invalid Scaling Strategy: " + ScalingStrategy);
                }

                var recalc = true;

                value = Math.Max(0, Math.Max(value0, value));

                while(spectrumPointIndex <= m_spectrumIndexMax.Length - 1 && i == (IsXLogScale ? m_spectrumLogScaleIndexMax[spectrumPointIndex] : m_spectrumIndexMax[spectrumPointIndex])) {
                    if(!recalc)
                        value = lastValue;

                    if(value > maxValue)
                        value = maxValue;

                    if(UseAverage && spectrumPointIndex > 0)
                        value = (lastValue + value) / 2d;

                    points.Add(new SpectrumPointData(spectrumPointIndex, value));
                    lastValue = value;
                    value = 0d;
                    spectrumPointIndex++;
                    recalc = false;
                }

                //value = 0;
            }

            return points;
        }

        protected virtual void SpectrumPointCalculated(float value, int index) { }

        protected virtual void Update() {
            CalculateSpectrumPoints();
            for(var i = 0; i < points.Count; i++)
                SpectrumPointCalculated((float)points[i].Value, points[i].SpectrumPointIndex);
        }

        protected virtual void OnEnable() {
            m_capture = new WasapiLoopbackCapture();
            m_capture.Initialize();

            SpectrumProvider = new SpectrumProvider(m_capture.WaveFormat.Channels, m_capture.WaveFormat.SampleRate, FftSize);

            var source = new SoundInSource(m_capture);
            var notificationSource = new SingleBlockNotificationStream(source.ToSampleSource());

            notificationSource.SingleBlockRead += (sender, args) => SpectrumProvider.Add(args.Left, args.Right);
            var finalSource = notificationSource.ToWaveSource();

            m_capture.DataAvailable += (sender, args) => finalSource.Read(args.Data, args.Offset, args.ByteCount);
            m_capture.Start();

            UpdateFrequencyMapping();
        }

        protected virtual void OnDisable() {
            if(m_capture != null) {
                m_capture.Stop();
                m_capture.Dispose();
            }
        }

        protected virtual void OnValidate() {
            OnDisable();
            OnEnable();
        }

    }
}